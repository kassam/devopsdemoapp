package com.example.demo;

import com.example.demo.controller.ApplicationUserController;
import com.example.demo.entity.ApplicationUser;
import com.example.demo.repo.ApplicationUserRepository;
import com.example.demo.service.ApplicationUserService;
import com.example.demo.util.ResponseObject;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

// SpringBootTest for integration test
@SpringBootTest(classes = DevOpsDemoApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class DemoApplicationTests {


    //  tested class should be annotated with @InjectMocks
    @InjectMocks
    private ApplicationUserController userController;

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    private ApplicationUserRepository repository;
    private ApplicationUserService service;

    @BeforeEach
    public void configureSUT() {
        // inner object in tested class
        // which passed to "verify" method should be mocked
        repository = mock(ApplicationUserRepository.class);
        // if we want test by service
        // service = mock(ApplicationUserService.class);
        service = new ApplicationUserService(repository);
        userController = new ApplicationUserController(service);
    }

    @Test
    public void test1() {
        assertTrue(
                this.restTemplate
                        .getForObject("http://localhost:" + port + "/app/home", String.class)
                        .startsWith("Hello"));
    }


    @Test
    public void testGetUserById() {
        ApplicationUser u = new ApplicationUser();
        u.setId(1);
        Optional<ApplicationUser> ou = Optional.of(u);

        when(repository.findById(1)).thenReturn(ou);
        ApplicationUser user = (ApplicationUser) userController.getById(1).getModel();
        verify(repository).findById(1);

        // if we want test by service
        /*
        when(service.getById(1)).thenReturn(ou);
        ApplicationUser user = (ApplicationUser) userController.getById(1).getModel();
        verify(service).getById(1);
        */

        assertEquals(1, user.getId());
    }


    // check first element if has id equals 1
    @Test
    public void testList() throws IOException {
        ResponseEntity<ResponseObject> response =
                restTemplate.getForEntity("http://localhost:" + port + "/applicationUser/get-all", ResponseObject.class);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        ObjectMapper objectMapper = new ObjectMapper();

        if (response.getBody() != null && !response.getBody().getList().isEmpty()) {

            String jsonStr = objectMapper.writeValueAsString(response.getBody().getList().get(0));
            ApplicationUser user = objectMapper.readValue(jsonStr, ApplicationUser.class);
            assertThat(user.getId(), is(1));
        }
    }
}
