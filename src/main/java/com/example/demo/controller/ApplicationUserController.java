package com.example.demo.controller;

import com.example.demo.entity.ApplicationUser;
import com.example.demo.repo.ApplicationUserRepository;
import com.example.demo.service.ApplicationUserService;
import com.example.demo.util.ResponseObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/applicationUser")
public class ApplicationUserController extends GenericController<ApplicationUserService, ApplicationUserRepository, ApplicationUser> {


    @Autowired
    private PasswordEncoder passwordEncoder;

    public ApplicationUserController(ApplicationUserService service) {
        super(service);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseObject insert(@RequestBody ApplicationUser user) {

        // encrypt the password
        String encryptedPassword = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptedPassword);

        return service.insert(user) != null ?
                new ResponseObject(ResponseObject.Text.ADDEDSUCCESSFULY, ResponseObject.ReturnCode.SUCCESS, null, user) :
                new ResponseObject(ResponseObject.Text.ADDINGFAILED, ResponseObject.ReturnCode.FAILED, null, null);
    }

}
