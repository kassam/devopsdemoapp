package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/app")
public class HomeController {

    @Autowired
    Environment env;

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(){
        return "Hello from DevOps Demo on port:" + env.getProperty("server.port");
    }


    @RequestMapping(value = "/print", method = RequestMethod.POST)
    public String print(@RequestParam("str") String str){
        return str;
    }
}
