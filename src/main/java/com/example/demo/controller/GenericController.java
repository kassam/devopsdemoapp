package com.example.demo.controller;

import com.example.demo.entity.GenericDomain;
import com.example.demo.util.ResponseObject;
import com.example.demo.service.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
public class GenericController<
        Service extends GenericService<Repository,Domain>,
        Repository extends JpaRepository<Domain,Integer>,
        Domain extends GenericDomain> {

    @Autowired
    protected Service service;

    public GenericController(Service service){
        this.service = service;
    }


    @RequestMapping(method = RequestMethod.GET, path = "/get-all")
    public ResponseObject getAll() {
        List<Domain> list = service.getAll();
        return list != null ? new ResponseObject(ResponseObject.Text.FETCHEDSUCCESSFULY, ResponseObject.ReturnCode.SUCCESS, list, null) :
                new ResponseObject(ResponseObject.Text.FETCHINGFAILED, ResponseObject.ReturnCode.FAILED, null, null);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/by-id")
    public ResponseObject getById(@RequestParam("id") Integer id) {
        Domain model = service.getById(id).get();
        return model != null ? new ResponseObject(ResponseObject.Text.FETCHEDSUCCESSFULY, ResponseObject.ReturnCode.SUCCESS, null, model) :
                new ResponseObject(ResponseObject.Text.FETCHINGFAILED, ResponseObject.ReturnCode.FAILED, null, null);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseObject insert(@RequestBody Domain domain) {
        return service.insert(domain) != null ?
                new ResponseObject(ResponseObject.Text.ADDEDSUCCESSFULY, ResponseObject.ReturnCode.SUCCESS, null, domain) :
                new ResponseObject(ResponseObject.Text.ADDINGFAILED, ResponseObject.ReturnCode.FAILED, null, null);
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/update")
    public ResponseObject update(@RequestBody Domain domain) {
        return service.update(domain) != null ?
                new ResponseObject(ResponseObject.Text.UPDATEDSUCCESSFULY, ResponseObject.ReturnCode.SUCCESS, null, domain) :
                new ResponseObject(ResponseObject.Text.UPDATINGFAILED, ResponseObject.ReturnCode.FAILED, null, null);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/delete")
    public ResponseObject delete(@RequestParam("id") Integer id) {
        return service.delete(id) ?
                new ResponseObject(ResponseObject.Text.DELETEDSUCCESSFULLY, ResponseObject.ReturnCode.SUCCESS, null, null) :
                new ResponseObject(ResponseObject.Text.DELETINGFAILED, ResponseObject.ReturnCode.FAILED, null, null);
    }
}
