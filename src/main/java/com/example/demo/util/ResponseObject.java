package com.example.demo.util;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseObject<Domain> implements Serializable {
    public enum ReturnCode {
        INFO("Info"),
        SUCCESS("Success"),
        FAILED("Failed"),
        WARNING("Warning");

        private String returnCode;

        ReturnCode(String returnCode) {
            this.returnCode = returnCode;
        }

        public String getReturnCode() {
            return returnCode;
        }
    }

    public enum Text {
        BADCREDENTIALS("Bad Credentials, 'domainName' and 'password' are required"),
        LOGGEDIN("you logged in successfully"),
        ADDEDSUCCESSFULY("addedSuccessfully"),
        ADDINGFAILED("addingFAILED"),
        UPDATEDSUCCESSFULY("updatedSuccessfully"),
        UPDATINGFAILED("updatingFAILED"),
        LUNCHEDSUCCESSFULY("Lunched Successfully"),
        LUNCHINGFFAILED("Lunching Failed"),
        DIPATCHEDSUCCESSFULY("Dispatched Successfully"),
        DISPATCHINGFAILED("Dispatching Failed"),
        CLOSEDSUCCESSFULY("Closed Successfully"),
        CLOSINGFAILED("Closing FAILED"),
        FETCHEDSUCCESSFULY("Fetched Successfully"),
        FETCHINGFAILED("Fetching failed"),
        DELETEDSUCCESSFULLY("Deleted Successfully"),
        DELETINGFAILED("Deleting failed"),
        UPLOADEDSUCCESSFULLY("Uploaded Successfully"),
        UPLOADINGFAILED("Uploaded failed"),
        SENTSUCCESSFULLY("Sent Successfully"),
        SENDINGFAILED("Sending Failed"),
        IMAGE("Image"),
        PDF("Pdf"),
        IMAGEANDPDF("Image and Pdf");

        private String text;

        Text(String text) {
            this.text = text;
        }

        public String getText() {
            return text;
        }
    }

    private String text;
    private String returnCode;
    private List<Domain> list;
    private Domain model;
    private Map<String, Object> extraData;

    public ResponseObject() {
        super();
        this.extraData = new HashMap<String, Object>();
    }

    public ResponseObject(Text text, ReturnCode returnCode) {
        this.text = text.getText();
        this.returnCode = returnCode.getReturnCode();
        this.extraData = new HashMap<String, Object>();
    }

    public ResponseObject(Text text, ReturnCode returnCode, List<Domain> list, Domain model) {
        this.text = text.getText();
        this.returnCode = returnCode.getReturnCode();
        this.list = list;
        this.model = model;
        this.extraData = new HashMap<String, Object>();
    }

    // print as json
    @Override
    public String toString() {
        return
                "\"message\" : {" +
                        "\"text\":\"" + text + '\"' +
                        ", \"returnCode\":\"" + returnCode + '\"' +
                        "} ";

    }

    @JsonAnySetter
    public void addExtraData(String key, Object value) {
        if (value != null)
            this.extraData.put(key, value);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    @JsonIgnoreProperties
    public List<Domain> getList() {
        return list;
    }

    public void setList(List<Domain> list) {
        this.list = list;
    }

    public Domain getModel() {
        return model;
    }

    public void setModel(Domain model) {
        this.model = model;
    }

    @JsonAnyGetter
    public Map<String, Object> getExtraData() {
        return extraData;
    }

    public void setExtraData(Map<String, Object> extraData) {
        this.extraData = extraData;
    }
}
