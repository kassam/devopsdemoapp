package com.example.demo.service;

import com.example.demo.entity.GenericDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Transactional(rollbackFor = Exception.class)
public class GenericService<
        Repository extends JpaRepository<Domain,Integer>,
        Domain extends GenericDomain> {

    @Autowired
    protected Repository repository;

    @Autowired
    GenericService(Repository repo){
        this.repository = repo;
    }

    @SuppressWarnings("unchecked")
    public void insert(List<Domain> list) {
        repository.saveAll(list);
    }

    public Domain insert(Domain domain) {
        try {
            domain =repository.save(domain);
            return domain;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Domain update(Domain domain) {
        try {
            domain =repository.save(domain);
            return domain;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean delete(Integer domainId) {
        try {
            repository.deleteById(domainId);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("unchecked")
    public List<Domain> getAll() {
        try {
            List<Domain> list = new ArrayList<Domain>();
            for (Domain bestDeal : repository.findAll()) {
                list.add(bestDeal);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    public Optional<Domain> getById(Integer id) {
        Optional model = repository.findById(id);
        if (!model.isPresent())
            return null;
        return model;
    }

    public Repository getRep() {
        return repository;
    }

    public void setRep(Repository repository) {
        this.repository = repository;
    }

}