package com.example.demo.service;

import com.example.demo.entity.ApplicationUser;
import com.example.demo.repo.ApplicationUserRepository;
import org.springframework.stereotype.Service;

@Service
public class ApplicationUserService extends GenericService<ApplicationUserRepository, ApplicationUser> {

    public ApplicationUserService(ApplicationUserRepository repo) {
        super(repo);
    }

    public ApplicationUser findByDomainName(String domainName) {
        return repository.findByDomainName(domainName);
    }
}